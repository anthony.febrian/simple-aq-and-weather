package com.rariki.simpleaqandweather

import android.app.Application
import com.rariki.airquality.di.AirQualityModules
import com.rariki.core.network.NetworkModules
import org.koin.android.BuildConfig
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())

        startKoin {
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            androidContext(this@BaseApplication)

            modules(
                listOf(
                    NetworkModules.modules,
                    AirQualityModules.modules,
                )
            )
        }
    }
}