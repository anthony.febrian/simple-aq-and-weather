package com.rariki.core.network

import okhttp3.Interceptor
import okhttp3.Response

class NetworkInterceptor:Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val builder = original.newBuilder()

        /**
         * You Can Add Header Or You Can modify Request Here
         */
        val url = chain
            .request()
            .url
            .newBuilder()
            .addQueryParameter("key", "15cf0958-7f22-4107-8391-adcf8e19c46a")
            .build()

        val request = builder
            .url(url)
            .build()
        return chain.proceed(request)
    }

}