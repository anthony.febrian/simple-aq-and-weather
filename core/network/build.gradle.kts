import com.rariki.buildsrc.Libs

plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
}

apply(from = "$rootDir/base-gradle.kts")

android {
    namespace = "com.rariki.core.network"
}

dependencies {

    api(Libs.retrofit)
    api(Libs.retrofitMoshi)
    implementation(platform(Libs.okHttpBoM))
    implementation(Libs.okHttpInterceptor)

    implementation(Libs.koin)

    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
}