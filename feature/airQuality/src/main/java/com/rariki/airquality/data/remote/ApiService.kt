package com.rariki.airquality.data.remote

import com.rariki.airquality.data.entity.response.ApiResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("v2/nearest_city")
    suspend fun getData(
        @Query("lat") lat: String,
        @Query("lon") lng: String,
    ): Response<ApiResponse>
}