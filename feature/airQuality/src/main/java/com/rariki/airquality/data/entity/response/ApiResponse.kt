package com.rariki.airquality.data.entity.response

data class ApiResponse(
    val status:String?,
    val data: DataResponse?
)