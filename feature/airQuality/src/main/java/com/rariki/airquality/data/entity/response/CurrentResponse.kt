package com.rariki.airquality.data.entity.response

data class CurrentResponse(
    val pollution: PollutionResponse?,
    val weather: WeatherResponse?
)