package com.rariki.airquality.data.entity.response

data class LocationResponse(
    val coordinates: List<Double>?,
    val type: String?,
)