package com.rariki.airquality.presentation.uistate

import android.location.Location


sealed class HomeUiState {
    object NotGranted:HomeUiState()
    data class Granted(
        val location: Location? = null,
    ):HomeUiState()
}