package com.rariki.airquality.data.entity.response

data class WeatherResponse(
    val hu: Int?,
    val ic: String?,
    val pr: Int?,
    val tp: Int?,
    val ts: String?,
    val wd: Int?,
    val ws: Double?
)