package com.rariki.airquality.di

import com.rariki.airquality.data.remote.ApiService
import com.rariki.airquality.presentation.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.dsl.module
import retrofit2.Retrofit

object AirQualityModules {
    val modules = module {

        single {
            get<Retrofit>()
                .create(ApiService::class.java)
        }

        viewModelOf(::HomeViewModel)
    }
}