package com.rariki.airquality.presentation.viewmodel

import android.location.Location
import androidx.lifecycle.ViewModel
import com.rariki.airquality.presentation.uistate.HomeUiState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

class HomeViewModel(
) : ViewModel() {

    private val _uiState = MutableStateFlow<HomeUiState>(HomeUiState.NotGranted)
    val uiState = _uiState.asStateFlow()

    init {
    }

    fun setIsLocationGranted(isGranted:Boolean) {
        _uiState.update {
            if (isGranted)
                HomeUiState.Granted()
            else
                HomeUiState.NotGranted
        }
    }

    fun setLocation(location: Location) {
        val uiState = uiState.value
        if(uiState is HomeUiState.Granted) {
            _uiState.update {
                uiState.copy(location = location)
            }
        }
    }
}