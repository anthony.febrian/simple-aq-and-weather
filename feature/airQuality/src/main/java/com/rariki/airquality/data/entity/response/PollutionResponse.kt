package com.rariki.airquality.data.entity.response

data class PollutionResponse(
    val aqicn: Int?,
    val aqius: Int?,
    val maincn: String?,
    val mainus: String?,
    val ts: String?,
)