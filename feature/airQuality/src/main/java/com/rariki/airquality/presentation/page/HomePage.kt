package com.rariki.airquality.presentation.page

import android.Manifest
import android.annotation.SuppressLint
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.rememberPermissionState
import com.google.android.gms.location.LocationServices
import com.rariki.airquality.presentation.uistate.HomeUiState
import com.rariki.airquality.presentation.viewmodel.HomeViewModel
import com.rariki.core.language.R
import com.rariki.core.ui.MyTheme
import org.koin.androidx.compose.koinViewModel
import timber.log.Timber

@SuppressLint("MissingPermission")
@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun HomePage(vm: HomeViewModel = koinViewModel()) {

    val context = LocalContext.current
    val locationPermissionState = rememberPermissionState(
        Manifest.permission.ACCESS_FINE_LOCATION,
        onPermissionResult = vm::setIsLocationGranted,
    )

    if(!locationPermissionState.status.isGranted) {
        LaunchedEffect(key1 = Unit) {
            locationPermissionState.launchPermissionRequest()
        }
    } else {
        val fusedLocationProviderClient =
            remember { LocationServices.getFusedLocationProviderClient(context) }

        fusedLocationProviderClient.lastLocation
            .addOnCompleteListener {
                vm.setLocation(it.result)
            }
    }


    val uiState by vm.uiState.collectAsState()
    HomeContent(
        uiState = uiState,
    )
}

@Composable
private fun HomeContent(
    uiState: HomeUiState,
) {
    when(uiState) {
        is HomeUiState.Granted -> Text(text = "Granted")
        HomeUiState.NotGranted -> Text(text = "Not Granted")
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = stringResource(id = R.string.hello, name),
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    MyTheme {
        Greeting("Android")
    }
}