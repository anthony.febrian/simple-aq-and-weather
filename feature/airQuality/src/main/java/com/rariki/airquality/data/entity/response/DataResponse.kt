package com.rariki.airquality.data.entity.response

data class DataResponse(
    val city: String?,
    val country: String?,
    val current: CurrentResponse?,
    val location: LocationResponse?,
    val state: String?,
)