import com.rariki.buildsrc.Versions
import com.rariki.buildsrc.Libs
import com.rariki.buildsrc.Projects


plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
}

apply(from = "$rootDir/base-gradle.kts")

android {
    namespace = "com.rariki.airquality"

    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = Versions.KOTLIN_COMPILER_EXTENSION
    }
}

dependencies {
    /**
     * CORE
     */
    implementation(project(Projects.CORE_UI))
    implementation(project(Projects.CORE_DATA))
    implementation(project(Projects.CORE_LANGUAGE))
    implementation(project(Projects.CORE_NETWORK))
    implementation(project(Projects.CORE_NAVIGATION))


    implementation(Libs.accompanistPermission)
    implementation(Libs.androidCoreKtx)
    implementation(Libs.composeActivity)
    implementation(platform(Libs.composeBoM))
    implementation(Libs.compose)
    implementation(Libs.composeToolingPreview)
    implementation(Libs.composeMaterial3)

    implementation(Libs.koinCompose)

    implementation(Libs.lifecyleViewModelCompose)

    implementation(Libs.timber)
    implementation("com.google.android.gms:play-services-location:21.0.1")



    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
}