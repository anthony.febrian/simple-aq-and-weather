pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "Simple Aq And Weather"
include(":app")
include(":core:ui")
include(":core:data")
include(":core:language")
include(":core:network")
include(":core:navigation")
include(":feature:airQuality")
