package com.rariki.buildsrc

object TestLibs {

    /**
     * Room
     */
    const val roomTesting = "androidx.room:room-testing:${Versions.ROOM}"
}